/* plugins.c  --  Functions for managing eurephia plug-ins
 *
 *  GPLv2 only - Copyright (C) 2009 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   sqlite/administration/plugins.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-03-28
 *
 * @brief  Functions for managing eurephia plug-ins
 *
 */

#include <string.h>
#include <unistd.h>
#include <assert.h>

#include <libxml/tree.h>

#include <sqlite3.h>

#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_xml.h>
#include <eurephia_values.h>
#include <eurephiadb_session_struct.h>
#include <eurephiadb_mapping.h>
#include <passwd.h>

#include "../sqlite.h"

#define FMAP_PLUGINS
#include "../fieldmapping.h"


/**
 * Internal function.  Queries the database for a list of plug-ins
 *
 * @param ctx      eurephiaCTX
 * @param fmap     eDBfieldMap containing the search criteria
 *
 * @return Returns a valid eurephia XML document on success, otherwise NULL
 */
static xmlDoc *plugins_search(eurephiaCTX *ctx, eDBfieldMap *fmap)
{
        dbresult *res = NULL;
        xmlDoc *doc = NULL;
        xmlNode *root_n = NULL, *rec_n = NULL;
        int i = 0;

        // Query the database for accesses
        res = sqlite_query_mapped(ctx, SQL_SELECT,
                                  "SELECT plgname, plgtype, plgdsofile, plgconfig,"
                                  "       plgenabled, plgid"
                                  "  FROM eurephia_plugins",
                                  NULL, fmap, "plgname");
        if( sqlite_query_status(res) != dbSUCCESS ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Error querying the database for plugins");
                sqlite_log_error(ctx, res);
                sqlite_free_results(res);
                return NULL;
        }

        eurephiaXML_CreateDoc(ctx, 1, "plugins", &doc, &root_n);
        xmlNewProp(root_n, (xmlChar *) "mode", (xmlChar *) "list");

        for( i = 0; i < sqlite_get_numtuples(res); i++ ) {
                rec_n = xmlNewChild(root_n, NULL, (xmlChar *) "plugin", NULL);
                sqlite_xml_value(rec_n, XML_ATTR, "plgid", res, i, 5);
                sqlite_xml_value(rec_n, XML_ATTR, "enabled", res, i, 4);
                sqlite_xml_value(rec_n, XML_NODE, "name", res, i, 0);
                sqlite_xml_value(rec_n, XML_NODE, "type", res, i, 1);
                sqlite_xml_value(rec_n, XML_NODE, "dsofile", res, i, 2);
                sqlite_xml_value(rec_n, XML_NODE, "config", res, i, 3);
        }
        sqlite_free_results(res);
        return doc;
}


/**
 * Internal function.  Registers a new plug-in
 *
 * @param ctx   eurephiaCTX
 * @param fmap  eDBfieldMap containing information about the new plug-in
 *
 * @return Returns an eurephia ResultMsg XML document, with success message or an error message
 */
static xmlDoc *plugins_register(eurephiaCTX *ctx, eDBfieldMap *fmap) {
        dbresult *res = NULL;
        xmlDoc *ret = NULL;

        // Check if we have the needed fields, and only the needed fields
        if( (eDBmappingFieldsPresent(fmap) & (FIELD_DESCR | FIELD_TYPE | FIELD_FILE)) == 0 ) {
                return eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                             "Registering a plug-in requires plug-in name, type and DSO file name");
        }

        if( eDBmappingGetValue(fmap, FIELD_ACTIVATED) == NULL ) {
                eDBmappingSetValue(fmap, FIELD_ACTIVATED, "t");
        }

        res = sqlite_query_mapped(ctx, SQL_INSERT, "INSERT INTO eurephia_plugins", fmap, NULL, NULL);
        if( sqlite_query_status(res) != dbSUCCESS ) {
                xmlNode *err_n = NULL;

                eurephia_log(ctx, LOG_FATAL, 0, "Could not register the new plug-in");
                err_n = sqlite_log_error_xml(ctx, res);
                ret = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n,
                                            "Could not register the the new plug-in");
                xmlFreeNode(err_n);
        } else {
                ret = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL,
                                            "Plug-in registered with id %i",
                                            res->last_insert_id);
        }
        sqlite_free_results(res);
        return ret;
}


/**
 * Internal function.  Deletes plug-in entry from the database
 *
 * @param ctx   eurephiaCTX
 * @param fmap  eDBfieldMap containing information about the plug-in(s) to be deleted
 *
 * @return Returns an eurephia ResultMsg XML document, with success message or an error message
 */
static xmlDoc *plugins_unregister(eurephiaCTX *ctx, eDBfieldMap *fmap)
{
        dbresult *res = NULL;
        xmlDoc *ret = NULL;
        xmlNode *err_n = NULL;
        long int fields;

        // Check if we have the needed fields, and only the needed fields
        fields = eDBmappingFieldsPresent(fmap);
        if( !(fields & FIELD_FILE) && !(fields & FIELD_RECID) ) {
                return eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                             "Deleting plug-ins only accepts "
                                             "DSO filename or plug-in ID");
        }

        // Find the accessprofile ID based on the fieldmap
        res = sqlite_query_mapped(ctx, SQL_SELECT,
                                  "SELECT DISTINCT authplugin"
                                  "  FROM openvpn_usercerts"
                                  "  JOIN eurephia_plugins ON (plgid = authplugin)",
                                  NULL, fmap, NULL);
        if( sqlite_query_status(res) != dbSUCCESS ) {
                eurephia_log(ctx, LOG_FATAL, 0, "Could not delete the plug-in (1)");
                err_n = sqlite_log_error_xml(ctx, res);
                ret = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n,
                                            "Could not delete the plug-in");
                xmlFreeNode(err_n);
                goto exit;
        }

        // Remove the usage for this plug-in from all user accounts using it
        if( sqlite_get_numtuples(res) > 0 ) {
                dbresult *dres = NULL;
                int i = 0;

                for( i = 0; i < sqlite_get_numtuples(res); i++ ) {
                        dres = sqlite_query(ctx,
                                            "UPDATE openvpn_usercerts "
                                            "   SET authplugin = NULL"
                                            " WHERE authplugin = %q",
                                            sqlite_get_value(res, i, 0));
                        if( sqlite_query_status(dres) != dbSUCCESS ) {
                                eurephia_log(ctx, LOG_FATAL, 0,
                                             "Could not remove the plug-in references");
                                err_n = sqlite_log_error_xml(ctx, res);
                                ret = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n,
                                                            "Could not remove the plug-in references");
                                sqlite_free_results(dres);
                                xmlFreeNode(err_n);
                                goto exit;
                        }
                }
                sqlite_free_results(dres);
        }

        // Delete requested access profiles from eurephia_plugins
        res = sqlite_query_mapped(ctx, SQL_DELETE, "DELETE FROM eurephia_plugins",
                                  NULL, fmap, NULL);
        if( sqlite_query_status(res) != dbSUCCESS ) {
                eurephia_log(ctx, LOG_FATAL, 0, "Could not delete the plug-in registration");
                err_n = sqlite_log_error_xml(ctx, res);
                ret = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n,
                                            "Could not delete the plug-in registration");
                xmlFreeNode(err_n);
        } else {
                ret = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL, "Plug-in unregistered");
        }
 exit:
        sqlite_free_results(res);
        return ret;
}


static xmlDoc * plugins_modify(eurephiaCTX *ctx, const char *plgid, const char *plgdso,
                               eDBfieldMap *vals_fmap)
{
        dbresult *dbres = NULL;
        xmlDoc *ret = NULL, *where_d = NULL;
        xmlNode *where_n = NULL;
        eDBfieldMap *where_m = NULL;
        long int fields;

        if( plgid == NULL && plgdso == NULL ) {
                return eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                             "This operation requires the plugin-id or plugin-dso"
                                             " attributes to be present");
        }

        // Check if we have the needed fields, and only the needed fields
        fields = eDBmappingFieldsPresent(vals_fmap);
        if( !(fields & (FIELD_CONFIG|FIELD_ACTIVATED))) {
                return eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                             "This operation requires configuration string or the enabled flag");
        }

        // Create a field-map for the where clause
        eurephiaXML_CreateDoc(ctx, 1, "plugins", &where_d, &where_n);
        assert( (where_d != NULL) && (where_n != NULL) );

        where_n = xmlNewChild(where_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(where_n, (xmlChar *) "table", (xmlChar *) "plugins");
        if( plgid != NULL ) {
                xmlNewChild(where_n, NULL, (xmlChar *) "plugin_id", (xmlChar *) plgid);
        }
        if( plgdso != NULL ) {
                xmlNewChild(where_n, NULL, (xmlChar *) "dsofile", (xmlChar *) plgdso);
        }
        where_m = eDBxmlMapping(ctx, tbl_sqlite_plugins, NULL, where_n);
        assert( where_m != NULL );

        dbres = sqlite_query_mapped(ctx, SQL_UPDATE, "UPDATE eurephia_plugins",
                                  vals_fmap, where_m, NULL);
        if( sqlite_query_status(dbres) == dbSUCCESS ) {
                int num_rows = sqlite_get_affected_rows(dbres);
                if( num_rows > 0 ) {
                        ret = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL,
                                                    "%i plug-in %s was updated",
                                                    num_rows,
                                                    (num_rows == 1 ? "record" : "records"));
                } else {
                        ret = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL,
                                                    "No plug-in records where modified");
                }
        } else {
                xmlNode *err_n = NULL;

                eurephia_log(ctx, LOG_ERROR, 0, "Failed to update user-cert link.(%s: %s)",
                             (plgid != NULL ? "plug-in ID" : "plug-in file"),
                             (plgid != NULL ? plgid : plgdso));
                err_n = sqlite_log_error_xml(ctx, dbres);
                ret = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n,
                                            "Failed to update user-cert link.(%s: %s)",
                                            (plgid != NULL ? "plug-in ID" : "plug-in file"),
                                            (plgid != NULL ? plgid : plgdso));
                xmlFreeNode(err_n);
        }
        sqlite_free_results(dbres);
        eDBfreeMapping(where_m);
        xmlFreeDoc(where_d);

        return ret;
}


/**
 * @copydoc eDBadminPlugins()
 */
xmlDoc *eDBadminPlugins(eurephiaCTX *ctx, xmlDoc *xmlqry)
{
        eDBfieldMap *fmap = NULL;
        char *mode = NULL;
        xmlDoc *resxml = NULL;
        xmlNode *root_n = NULL, *fieldmap_n = NULL;

        DEBUG(ctx, 20, "Function call: eDBadminPlugins(ctx, {xmlDoc})");
        assert( (ctx != NULL) && (xmlqry != NULL) );

        if( (ctx->context_type != ECTX_ADMIN_CONSOLE) && (ctx->context_type != ECTX_ADMIN_WEB) ) {
                eurephia_log(ctx, LOG_CRITICAL, 0,
                             "eurephia admin function call attempted with wrong context type");
                return NULL;
        }

        root_n = eurephiaXML_getRoot(ctx, xmlqry, "plugins", 1);
        if( root_n == NULL ) {
                eurephia_log(ctx, LOG_CRITICAL, 0, "Invalid XML input.");
                return NULL;
        }
        mode = xmlGetAttrValue(root_n->properties, "mode");
        if( mode == NULL ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Missing mode attribute");
                return NULL;
        }

        fieldmap_n = xmlFindNode(root_n, "fieldMapping");
        if( fieldmap_n == NULL ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Missing fieldMapping");
        }
        fmap = eDBxmlMapping(ctx, tbl_sqlite_plugins, NULL, fieldmap_n);

        if( strcmp(mode, "search") == 0 ) {
                resxml = plugins_search(ctx, fmap);
        } else if( strcmp(mode, "register") == 0 ) {
                resxml = plugins_register(ctx, fmap);
        } else if( strcmp(mode, "unregister") == 0 ) {
                resxml = plugins_unregister(ctx, fmap);
        } else if( strcmp(mode, "modify") == 0 ) {
                char *plgid = xmlGetAttrValue(root_n->properties, "plugin-id");
                char *plgdso = xmlGetAttrValue(root_n->properties, "plugin-dso");
                resxml = plugins_modify(ctx, plgid, plgdso, fmap);
        } else {
                eurephia_log(ctx, LOG_ERROR, 0, "Plug-ins - Unknown mode: '%s'", mode);
                resxml = eurephiaXML_ResultMsg(ctx, exmlERROR, NULL, "Unknown mode '%s'", mode);
        }
        eDBfreeMapping(fmap);
        return resxml;
}
