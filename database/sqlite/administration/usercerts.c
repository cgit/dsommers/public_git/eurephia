/* usercerts.c  --  Admin functions - user-certificate table
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   sqlite/administration/usercerts.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-08-29
 *
 * @brief  Functions for working with the openvpn_usercerts table
 *
 */

#include <string.h>
#include <unistd.h>
#include <assert.h>

#include <libxml/tree.h>

#include <sqlite3.h>

#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_xml.h>
#include <eurephia_values.h>
#include <eurephiadb_session_struct.h>
#include <eurephiadb_mapping.h>

#include "../sqlite.h"

#define FMAP_USERCERTS
#include "../fieldmapping.h"


/**
 * Internal function.  Queries the database for a list of user-certificate links
 *
 * @param ctx      eurephiaCTX
 * @param where_m  eDBfieldMap containing the search criteria
 * @param sortkeys String containing the sort order of the data
 *
 * @return Returns a valid eurephia XML document on success, otherwise NULL
 */
xmlDoc *usercerts_search(eurephiaCTX *ctx, eDBfieldMap *where_m, const char *sortkeys) {
        xmlDoc *list_xml = NULL;
        xmlNode *link_root_n = NULL, *link_n = NULL, *tmp_n = NULL;
        dbresult *res = NULL;
        xmlChar tmp[2050];
        const char *dbsort = NULL;
        int i;

        DEBUG(ctx, 21, "Function call: usercerts_search(ctx, eDBfieldMap, '%s')", sortkeys);
        assert( ctx != NULL );

        if( (ctx->context_type != ECTX_ADMIN_CONSOLE) && (ctx->context_type != ECTX_ADMIN_WEB) ) {
                eurephia_log(ctx, LOG_CRITICAL, 0,
                             "eurephia admin function call attempted with wrong context type");
                return NULL;
        }

        if( sortkeys != NULL ) {
                dbsort = eDBmkSortKeyString(where_m, sortkeys);
        }

        res = sqlite_query_mapped(ctx, SQL_SELECT,
                                  "SELECT uicid, ucs.uid AS uid, certid, locdt(ucs.registered) AS registered,"
                                  "       ucs.accessprofile AS accessprofile, access_descr,"
                                  "       username, "
                                  "       common_name, organisation, email, lower(digest), depth, "
                                  "       authplugin, plgname, authusername"
                                  "  FROM openvpn_usercerts ucs"
                                  "  LEFT JOIN openvpn_certificates USING(certid)"
                                  "  LEFT JOIN openvpn_accesses acc ON(ucs.accessprofile = acc.accessprofile)"
                                  "  LEFT JOIN openvpn_users u ON(u.uid = ucs.uid)"
                                  "  LEFT JOIN eurephia_plugins plg ON(ucs.authplugin = plg.plgid)",
                                  NULL, // values (not used for SELECT)
                                  where_m, // fields and values for the WHERE clause
                                  dbsort);
        if( sqlite_query_status(res) != dbSUCCESS ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Could not query the usercerts table");
                sqlite_log_error(ctx, res);
                sqlite_free_results(res);
                return NULL;
        }

        memset(&tmp, 0, 2050);
        eurephiaXML_CreateDoc(ctx, 1, "usercerts", &list_xml, &link_root_n);
        xmlStrPrintf(tmp, 64, (xmlChar *) "%i", sqlite_get_numtuples(res));
        xmlNewProp(link_root_n, (xmlChar *) "link_count", (xmlChar *) tmp);

        for( i = 0; i < sqlite_get_numtuples(res); i++ ) {
                link_n = xmlNewChild(link_root_n, NULL, (xmlChar *) "usercert_link", NULL);

                sqlite_xml_value(link_n, XML_ATTR, "uicid", res, i, 0);
                sqlite_xml_value(link_n, XML_ATTR, "registered", res, i, 3);

                tmp_n = sqlite_xml_value(link_n, XML_NODE, "username", res, i, 6);
                sqlite_xml_value(tmp_n, XML_ATTR, "uid", res, i, 1);

                tmp_n = xmlNewChild(link_n, NULL, (xmlChar *) "certificate", NULL);
                sqlite_xml_value(tmp_n, XML_ATTR, "certid", res, i, 2);
                sqlite_xml_value(tmp_n, XML_ATTR, "depth", res, i, 11);

                xmlStrPrintf(tmp, 2048, (xmlChar *) "%.2048s", sqlite_get_value(res, i, 7));
                xmlReplaceChars(tmp, '_', ' ');
                xmlNewChild(tmp_n, NULL, (xmlChar *) "common_name", tmp);

                xmlStrPrintf(tmp, 2048, (xmlChar *) "%.2048s", sqlite_get_value(res, i, 8));
                xmlReplaceChars(tmp, '_', ' ');
                xmlNewChild(tmp_n, NULL, (xmlChar *) "organisation", tmp);

                sqlite_xml_value(tmp_n, XML_NODE, "email", res, i, 9);
                sqlite_xml_value(tmp_n, XML_NODE, "digest", res, i, 10);

                tmp_n = sqlite_xml_value(link_n, XML_NODE, "access_profile", res, i, 5);
                sqlite_xml_value(tmp_n, XML_ATTR, "accessprofile", res, i, 4);

                // Add optional authentication plug-in info
                if( atoi_nullsafe(sqlite_get_value(res, i, 12)) > 0 ) {
                        tmp_n = xmlNewChild(link_n, NULL, (xmlChar *) "authplugin", NULL);
                        sqlite_xml_value(tmp_n, XML_ATTR, "authplugid",    res, i, 12);
                        sqlite_xml_value(tmp_n, XML_NODE, "description",   res, i, 13);
                        sqlite_xml_value(tmp_n, XML_NODE, "auth_username", res, i, 14);
                }
        }
        sqlite_free_results(res);

        return list_xml;
}


/**
 * Internal function.  Adds or deletes a user-certificate link
 *
 * @param ctx       eurephiaCTX
 * @param mode      String containing the operation mode.  Must be "register" or "remove"
 * @param usrcrt_m  eDBfieldMap containing information about the link to add or remove
 *
 * @return Returns an eurephia ResultMsg XML document, with success message or an error message
 */
xmlDoc *usercerts_add_del(eurephiaCTX *ctx, const char *mode, eDBfieldMap *usrcrt_m) {
        xmlDoc *res = NULL;
        dbresult *dbres = NULL;

        DEBUG(ctx, 21, "Function call: usercerts_add_del(ctx, xmlDoc)");
        assert( (ctx != NULL) && (usrcrt_m != NULL) );

        if( strcmp(mode, "register") == 0 ) {
                dbres = sqlite_query_mapped(ctx, SQL_INSERT,
                                            "INSERT INTO openvpn_usercerts", usrcrt_m, NULL, NULL);
                if( sqlite_query_status(dbres) == dbSUCCESS ) {
                        res = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL,
                                                    "Registered new user-cert link with id %i",
                                                    dbres->last_insert_id);
                }
        } else if( strcmp(mode, "remove") == 0 ) {
                dbres = sqlite_query_mapped(ctx, SQL_DELETE,
                                            "DELETE FROM openvpn_usercerts", NULL, usrcrt_m, NULL);
                if( sqlite_query_status(dbres) == dbSUCCESS ) {
                        int num_rows = sqlite_get_affected_rows(dbres);
                        if( num_rows > 0 ) {
                                res = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL,
                                                            "Removed %i user-cert %s successfully",
                                                            num_rows, (num_rows == 1 ? "link" : "links"));
                        } else {
                                res = eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                                            "No user-cert links where removed");
                        }
                }
        }

        if( sqlite_query_status(dbres) != dbSUCCESS ) {
                xmlNode *tmp_n = NULL;

                eurephia_log(ctx, LOG_ERROR, 0, "Failed to %s user-cert link.", mode);
                tmp_n = sqlite_log_error_xml(ctx, dbres);
                res = eurephiaXML_ResultMsg(ctx, exmlERROR, tmp_n, "Failed to %s user-cert link", mode);
                xmlFreeNode(tmp_n);
        }
        sqlite_free_results(dbres);

        return res;
}


/**
 * Internal function.  Updates a user-certs link in the database
 *
 * @param ctx      eurephiaCTX
 * @param uicid    String containing the numeric record ID (uicid field) of the record to be updated
 * @param usrcrt_m eDBfieldMap containing the new information for the record.  Only the fields being
 *                 changed needs to be set.
 *
 * @return Returns an eurephia ResultMsg XML document, with success message or an error message
 */
xmlDoc *usercerts_update(eurephiaCTX *ctx, const char *uicid, eDBfieldMap *usrcrt_m) {
        xmlNode *where_n = NULL;
        eDBfieldMap *where_m = NULL;
        dbresult *dbres = NULL;
        xmlDoc *where_d = NULL, *res = NULL;

        DEBUG(ctx, 21, "Function call: usercerts_update(ctx, '%s', eDBfieldMap)", uicid);
        assert( ctx != NULL && uicid != NULL && usrcrt_m != NULL );

        // Create a eDBfieldMap which will contain the uicid value
        eurephiaXML_CreateDoc(ctx, 1, "usercerts", &where_d, &where_n);
        assert( (where_d != NULL) && (where_n != NULL) );

        where_n = xmlNewChild(where_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(where_n, (xmlChar *) "table", (xmlChar *) "usercerts");
        xmlNewChild(where_n, NULL, (xmlChar *) "uicid", (xmlChar *) uicid);

        // Convert xmlNode with fieldMapping into a eDBfieldMap
        where_m = eDBxmlMapping(ctx, tbl_sqlite_usercerts, NULL, where_n);
        assert( where_m != NULL );

        // Send update query to the database
        dbres = sqlite_query_mapped(ctx, SQL_UPDATE, "UPDATE openvpn_usercerts",
                                    usrcrt_m, where_m, NULL);
        if( sqlite_query_status(dbres) == dbSUCCESS ) {
                int num_rows = sqlite_get_affected_rows(dbres);
                if( num_rows > 0 ) {
                        res = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL,
                                                    "Updated firewall access profile on %i user-cert %s.",
                                                    num_rows, (num_rows == 1 ? "link" : "links"));
                } else {
                        res = eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                                    "No user-cert links where updated");
                }
        } else {
                xmlNode *err_n = NULL;

                eurephia_log(ctx, LOG_ERROR, 0, "Failed to update user-cert link.(uicid: %s)", uicid);
                err_n = sqlite_log_error_xml(ctx, dbres);
                res = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n,
                                            "Failed to update user-cert link for uicid %s", uicid);
                xmlFreeNode(err_n);
        }
        sqlite_free_results(dbres);
        eDBfreeMapping(where_m);
        xmlFreeDoc(where_d);

        return res;
}


/**
 * @copydoc eDBadminUserCertsLink()
 */
xmlDoc *eDBadminUserCertsLink(eurephiaCTX *ctx, xmlDoc *usrcrt_xml) {
        xmlNode *usrcrt_n = NULL, *tmp_n = NULL;
        xmlDoc *resxml = NULL;
        eDBfieldMap *usrcrt_m = NULL;
        const char *mode = NULL, *sortfields = NULL, *uicid = NULL;

        DEBUG(ctx, 20, "Function call: eDBadminUserCertsLink(ctx, xmlDoc)");
        assert( (ctx != NULL) && (usrcrt_xml != NULL) );

        if( (ctx->context_type != ECTX_ADMIN_CONSOLE) && (ctx->context_type != ECTX_ADMIN_WEB) ) {
                eurephia_log(ctx, LOG_CRITICAL, 0,
                             "eurephia admin function call attempted with wrong context type");
                return 0;
        }

        usrcrt_n = eurephiaXML_getRoot(ctx, usrcrt_xml, "usercerts", 1);
        if( usrcrt_n == NULL ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Could not find a valid XML for the user-certs link request");
                return 0;
        }
        mode = xmlGetAttrValue(usrcrt_n->properties, "mode");
        if( mode == NULL ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Invalid user-cert link request (1).");
                return 0;
        }

        tmp_n = xmlFindNode(usrcrt_n, "sortfields");
        if( tmp_n ) {
                sortfields = xmlExtractContent(tmp_n);
        }

        tmp_n = xmlFindNode(usrcrt_n, "fieldMapping");
        if( tmp_n == NULL ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Invalid user-cert link request (2).");
                return 0;
        }
        usrcrt_m = eDBxmlMapping(ctx, tbl_sqlite_usercerts, NULL, tmp_n);
        assert(usrcrt_m != NULL);


        if( strcmp(mode, "search") == 0 ) {
                resxml = usercerts_search(ctx, usrcrt_m, sortfields);
        } else if( strcmp(mode, "register") == 0 ) {
                resxml = usercerts_add_del(ctx, mode, usrcrt_m);
        } else if( strcmp(mode, "remove") == 0 ) {
                resxml = usercerts_add_del(ctx, mode, usrcrt_m);
        } else if( strcmp(mode, "update") == 0 ) {
                uicid = xmlGetAttrValue(usrcrt_n->properties, "uicid");
                if( uicid == NULL ) {
                        eurephia_log(ctx, LOG_ERROR, 0, "Missing required attribute, uicid, for updates");
                        resxml = eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                                       "Can not set firewall access profile without uicid");
                        goto exit;
                }
                resxml = usercerts_update(ctx, uicid, usrcrt_m);
        }

 exit:
        eDBfreeMapping(usrcrt_m);
        return resxml;
}
