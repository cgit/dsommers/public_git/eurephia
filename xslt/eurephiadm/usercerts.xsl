<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2009 - 2012
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:apply-templates select="usercerts"/>
  </xsl:template>

  <xsl:template match="/eurephia/usercerts">
    <xsl:text> UICID - Registered&#10;</xsl:text>
    <xsl:text>         U: [uid]    Username   (Alt. auth username)&#10;</xsl:text>
    <xsl:text>         C: [certid] Common name/Organisation (cert.depth)&#10;</xsl:text>
    <xsl:if test="$firewall = '1'">
      <xsl:text>         A: [accessprofile] Access profile name&#10;</xsl:text>
    </xsl:if>
      <xsl:text>         P: [auth-plugin] Auth plug-in descr&#10;</xsl:text>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="usercert_link"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/eurephia/usercerts/usercert_link">
    <xsl:text> </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@uicid"/>
      <xsl:with-param name="width" select="5"/>
    </xsl:call-template>
    <xsl:text> - </xsl:text>
    <xsl:value-of select="@registered"/>
    <xsl:text>&#10;         U: [</xsl:text>

    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="username/@uid"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template>
    <xsl:text>] </xsl:text>
    <xsl:choose>
      <xsl:when test="username != ''"><xsl:value-of select="username"/></xsl:when>
      <xsl:otherwise>(Unknown user account)</xsl:otherwise>
    </xsl:choose>
    <xsl:if test="authplugin/auth_username">
      <xsl:text>   (</xsl:text>
      <xsl:value-of select="authplugin/auth_username"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
    <xsl:text>&#10;         C: [</xsl:text>

    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="certificate/@certid"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template>
    <xsl:text>] </xsl:text>
    <xsl:choose>
      <xsl:when test="certificate/@depth != ''">
        <xsl:value-of select="concat(certificate/common_name,'/',certificate/organisation, ' (',certificate/@depth,')')"/>
      </xsl:when>
      <xsl:otherwise>(Unknown certificate)</xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>

    <xsl:if test="$firewall = '1'">
      <xsl:text>         A: [</xsl:text>
      <xsl:call-template name="right-align">
        <xsl:with-param name="value" select="access_profile/@accessprofile"/>
        <xsl:with-param name="width" select="3"/>
      </xsl:call-template>
      <xsl:text>] </xsl:text>
      <xsl:value-of select="access_profile"/>
      <xsl:text>&#10;</xsl:text>
    </xsl:if>

    <xsl:if test="authplugin">
      <xsl:text>         P: [</xsl:text>
      <xsl:call-template name="right-align">
        <xsl:with-param name="value" select="authplugin/@authplugid"/>
        <xsl:with-param name="width" select="3"/>
      </xsl:call-template>
      <xsl:text>] </xsl:text>
      <xsl:value-of select="authplugin/description"/>
      <xsl:text>&#10;</xsl:text>
    </xsl:if>

    <xsl:if test="last() > position()">
      <xsl:text>&#10;</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), $value)"/>
  </xsl:template>

</xsl:stylesheet>
