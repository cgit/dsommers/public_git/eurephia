<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2009 - 2012
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:choose>
      <xsl:when test="$view = 'userlist'">
        <xsl:apply-templates select="UserAccount" mode="userlist"/>
      </xsl:when>
      <xsl:when test="$view = 'userinfo'">
        <xsl:apply-templates select="UserAccount/Account" mode="userinfo"/>
      </xsl:when>
      <xsl:when test="$view = 'attemptblacklist'">
        <xsl:apply-templates select="UserAccount/Account" mode="attemptblacklist"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">Invalid view: <xsl:value-of select="$view"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/eurephia/UserAccount" mode="userlist">
    <xsl:text>  ID Username             Activated        Deactivated      Last access&#10;</xsl:text>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="Account" mode="userlist"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/eurephia/UserAccount/Account" mode="userlist">
    <xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="@uid"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="username"/>
      <xsl:with-param name="width" select="20"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>

    <xsl:variable name="activated">
      <xsl:choose>
        <xsl:when test="activated != ''"><xsl:value-of select="activated"/></xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="$activated"/>
      <xsl:with-param name="width" select="16"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>

    <xsl:variable name="deactivated">
      <xsl:choose>
        <xsl:when test="deactivated != ''"><xsl:value-of select="deactivated"/></xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="$deactivated"/>
      <xsl:with-param name="width" select="16"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>

    <xsl:variable name="lastaccess">
      <xsl:choose>
        <xsl:when test="last_accessed != ''"><xsl:value-of select="last_accessed"/></xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="$lastaccess"/>
      <xsl:with-param name="width" select="16"/>
    </xsl:call-template>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/eurephia/UserAccount/Account" mode="userinfo">
    <xsl:text>                  User id: </xsl:text>
    <xsl:value-of select="@uid"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>                User name: </xsl:text>
    <xsl:value-of select="username"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>              Login count: </xsl:text>
    <xsl:choose>
      <xsl:when test="last_accessed != ''"><xsl:value-of select="@logincount"/></xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>            Last accessed: </xsl:text>
    <xsl:choose>
      <xsl:when test="last_accessed != ''"><xsl:value-of select="last_accessed"/></xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>                Activated: </xsl:text>
    <xsl:choose>
      <xsl:when test="activated != ''"><xsl:value-of select="activated"/></xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>               Dectivated: </xsl:text>
    <xsl:choose>
      <xsl:when test="deactivated != ''"><xsl:value-of select="deactivated"/></xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>                    Flags:</xsl:text>
    <xsl:choose>
      <xsl:when test="count(flags/flag) > 0">
        <xsl:for-each select="flags/flag">
          <xsl:text> </xsl:text><xsl:value-of select="."/>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise><xsl:text> -</xsl:text></xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/eurephia/UserAccount/Account" mode="attemptblacklist">
    <xsl:text>Login attempt information: </xsl:text>
    <xsl:choose>
      <xsl:when test="count(attempts/*) > 0">
        <xsl:text>&#10;          Failed attempts: </xsl:text>
        <xsl:value-of select="attempts/@attempts"/>
        <xsl:text>&#10;            First attempt: </xsl:text>
        <xsl:value-of select="attempts/first_attempt"/>
        <xsl:text>&#10;             Last attempt: </xsl:text>
        <xsl:value-of select="attempts/last_attempt"/>
        <xsl:text>&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Nothing registered</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>

    <xsl:text>&#10;</xsl:text>
    <xsl:text>    Blacklist information: </xsl:text>
    <xsl:choose>
      <xsl:when test="count(blacklist/*) > 0">
        <xsl:text>&#10;              Blacklisted: </xsl:text>
        <xsl:value-of select="blacklist/blacklisted"/>
        <xsl:text>&#10;             Last attempt: </xsl:text>
        <xsl:value-of select="blacklist/last_accessed"/>
        <xsl:text>&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Nothing registered</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>



  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), $value)"/>
  </xsl:template>

</xsl:stylesheet>
