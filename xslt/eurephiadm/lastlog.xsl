<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2008 - 2012
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:choose>
      <xsl:when test="$view = 'list'">
        <xsl:apply-templates select="lastlog|UserAccount/Account/lastlog" mode="list"/>
      </xsl:when>
      <xsl:when test="$view = 'details' or $view = 'details2'">
        <xsl:apply-templates select="lastlog|UserAccount/Account/lastlog" mode="details"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">Invalid view: <xsl:value-of select="$view"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="lastlog" mode="list">
    <xsl:param name="submode"/>

    <xsl:variable name="firstcol_label">
      <xsl:choose>
        <xsl:when test="name(../..) = 'UserAccount'">
          <xsl:text> Remote IP</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text> Username </xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:value-of select="$firstcol_label"/>
    <xsl:text>                Login                Logout                   Status&#10;</xsl:text>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="session" mode="list"/>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="lastlog/session" mode="list">
    <xsl:text> </xsl:text>
    <xsl:choose>
      <xsl:when test="name(../../..) = 'UserAccount'">
        <xsl:call-template name="left-align">
          <xsl:with-param name="value" select="connection/remote_host"/>
          <xsl:with-param name="width" select="24"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="left-align">
          <xsl:with-param name="value" select="username"/>
          <xsl:with-param name="width" select="24"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="login"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:variable name="logout">
      <xsl:choose>
        <xsl:when test="logout != ''"><xsl:value-of select="logout"/></xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="$logout"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template><xsl:text>  </xsl:text>

    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@session_status"/>
      <xsl:with-param name="width" select="10"/>
    </xsl:call-template>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>


  <xsl:template match="lastlog" mode="details">
    <xsl:text> Status     Login                                 Logout        Session closed&#10;</xsl:text>
    <xsl:text> Protocol   Remote:port                          VPN MAC                VPN IP&#10;</xsl:text>
    <xsl:choose>
      <xsl:when test="$view = 'details2'">
        <xsl:text> Common name / Username                                           Organisation&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text> Common name                                                      Organisation&#10;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="$firewall = '1'">
      <xsl:text> Firewall access profile                                        FW Destination&#10;</xsl:text>
    </xsl:if>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="session" mode="details"/>
    <xsl:text>-------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="lastlog/session" mode="details">
    <xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="@session_status"/>
      <xsl:with-param name="width" select="10"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="login"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template><xsl:text>      </xsl:text>

    <xsl:variable name="logout">
      <xsl:choose>
        <xsl:when test="logout != ''"><xsl:value-of select="logout"/></xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="$logout"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template><xsl:text>   </xsl:text>

    <xsl:variable name="sessionclose">
      <xsl:choose>
        <xsl:when test="session_closed != ''"><xsl:value-of select="session_closed"/></xsl:when>
        <xsl:otherwise>-</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="$sessionclose"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>&#10; </xsl:text>

    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="connection/protocol"/>
      <xsl:with-param name="width" select="11"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="concat(connection/remote_host,':',connection/remote_port)"/>
      <xsl:with-param name="width" select="22"/>
    </xsl:call-template>
    <xsl:text>  </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="connection/vpn_macaddr"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>   </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="connection/vpn_ipv4addr"/>
      <xsl:with-param name="width" select="19"/>
    </xsl:call-template>
    <xsl:text>&#10; </xsl:text>

    <xsl:choose>
      <xsl:when test="$view = 'details2'">
        <xsl:call-template name="left-align">
          <xsl:with-param name="value" select="concat(certificate/common_name,' / ',username)"/>
          <xsl:with-param name="width" select="37"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="left-align">
          <xsl:with-param name="value" select="certificate/common_name"/>
          <xsl:with-param name="width" select="37"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>   </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="certificate/organisation"/>
      <xsl:with-param name="width" select="37"/>
    </xsl:call-template>

    <xsl:if test="$firewall = '1'">
      <xsl:text>&#10; </xsl:text>
      <xsl:call-template name="left-align">
        <xsl:with-param name="value" select="certificate/access_profile"/>
        <xsl:with-param name="width" select="37"/>
      </xsl:call-template>
      <xsl:text>   </xsl:text>
      <xsl:call-template name="right-align">
        <xsl:with-param name="value" select="certificate/access_profile/@fwdestination"/>
        <xsl:with-param name="width" select="37"/>
      </xsl:call-template>
    </xsl:if>

    <xsl:text>&#10;</xsl:text>
    <xsl:if test="last() > position()">
          <xsl:text>&#10;</xsl:text>
    </xsl:if>
  </xsl:template>


  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), $value)"/>
  </xsl:template>

</xsl:stylesheet>
