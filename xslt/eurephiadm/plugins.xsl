<?xml version="1.0"?>
<!--
     *
     *  GPLv2 only - Copyright (C) 2013
     *               David Sommerseth <dazo@users.sourceforge.net>
     *
     *  This program is free software; you can redistribute it and/or
     *  modify it under the terms of the GNU General Public License
     *  as published by the Free Software Foundation; version 2
     *  of the License.
     *
     *  This program is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  You should have received a copy of the GNU General Public License
     *  along with this program; if not, write to the Free Software
     *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
     *
-->
<xsl:stylesheet  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/eurephia">
    <xsl:choose>
      <xsl:when test="$view = 'list'">
        <xsl:apply-templates select="plugins" mode="list"/>
      </xsl:when>
      <xsl:when test="$view = 'details'">
        <xsl:apply-templates select="plugins" mode="details"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">Invalid view: <xsl:value-of select="$view"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/eurephia/plugins" mode="list">
    <xsl:text>   ID Enab Type        Plug-in&#10;</xsl:text>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="plugin" mode="list"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/eurephia/plugins/plugin" mode="list">
    <xsl:text>  </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@plgid"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:choose>
      <xsl:when test="@enabled='t'">
        <xsl:text>  Y  </xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>     </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="type"/>
      <xsl:with-param name="width" select="11"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="name"/>
      <xsl:with-param name="width" select="56"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>


  <xsl:template match="/eurephia/plugins" mode="details">
    <xsl:text>   ID Enab Type        Plug-in DSO file&#10;</xsl:text>
    <xsl:text>           [Configuration]&#10;</xsl:text>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
    <xsl:apply-templates select="plugin" mode="details"/>
    <xsl:text> ------------------------------------------------------------------------------&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/eurephia/plugins/plugin" mode="details">
    <xsl:text>  </xsl:text>
    <xsl:call-template name="right-align">
      <xsl:with-param name="value" select="@plgid"/>
      <xsl:with-param name="width" select="3"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:choose>
      <xsl:when test="@enabled='t'">
        <xsl:text>  Y  </xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>     </xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="left-align">
      <xsl:with-param name="value" select="type"/>
      <xsl:with-param name="width" select="11"/>
    </xsl:call-template><xsl:text> </xsl:text>
    <xsl:value-of select="dsofile"/>
    <xsl:text>&#10;</xsl:text>
    <xsl:text>           [</xsl:text>
    <xsl:value-of select="config"/>
    <xsl:text>]&#10;</xsl:text>
    <xsl:if test="last() > position()">
          <xsl:text>&#10;</xsl:text>
    </xsl:if>
 </xsl:template>


  <xsl:template name="left-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="substring(concat($value, '                                                                                '), 1, $width)"/>
  </xsl:template>

  <xsl:template name="right-align">
    <xsl:param name="value"/>
    <xsl:param name="width"/>
    <xsl:value-of select="concat(substring('                                                                                ', 1, $width - string-length($value)), $value)"/>
  </xsl:template>

</xsl:stylesheet>
