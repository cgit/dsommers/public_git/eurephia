/* eurephiafw_helpers.h  --  Helper functions, shared between main module and
 *                           firewall module.  Setting up Posix MQ and semaphores
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiafw_helpers.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-08-14
 *
 * @brief  Helper functions which is shared between the main eurephia-auth module and
 *         the firewall module.  It takes care of preparing POSIX MQ queues and semaphores.
 *
 */

#ifndef   	EUREPHIAFW_HELPERS_H_
#define   	EUREPHIAFW_HELPERS_H_

#define EFW_MSG_SIZE  sizeof(eFWupdateRequest) /**< Maximum size of a message in the POSIX MQ queue */
#define MQUEUE_NAME "/eurephiaFW"        /**< Name of the MQ queue */
#define SEMPH_MASTER "eurephiafw_master" /**< Name of the semaphore the main openvpn process uses */
#define SEMPH_WORKER "eurephiafw_worker" /**< Name of the semaphore the firewall process uses */

int efwSetupSemaphores(eurephiaCTX *, efw_threaddata *);
int efwRemoveSemaphores(eurephiaCTX *, efw_threaddata *);

int efwSetupMessageQueue(eurephiaCTX *, efw_threaddata *);
int efwRemoveMessageQueue(eurephiaCTX *, efw_threaddata *);

#endif 	    /* !EUREPHIAFW_HELPERS_H_ */
