/* eurephiafw.h  --  Firewall interface struct typedef
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiafw_struct.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-11-05
 *
 * @brief  Declaraion of the constants and structs the firewall interface uses
 *
 */


#ifndef         EUREPHIAFW_STRUCT_H_
#define         EUREPHIAFW_STRUCT_H_

#include <mqueue.h>
#include <semaphore.h>

#include <eurephia_values_struct.h>

/**
 * Important variables for the firewall updater process.  They are sent to the
 * firewall process during the process forking.
 */
typedef struct {
        mqd_t msgq;             /**< MQ descriptor */
        sem_t *semp_worker;     /**< semaphore descriptor for the firewall process */
        sem_t *semp_master;     /**< semaphore descriptor for the main openvpn process */
        char *fw_command;       /**< string (char *) with full path to the firewall update command */
        void *ctx;              /**< A "shadow" eurephiaCTX used for allowing shared logging */
} efw_threaddata;

/**
 * Definition of the eurephiaFWINTF struct
 */
typedef struct {
        efw_threaddata thrdata;      /**< Process related information */
        pid_t fwproc_pid;            /**< Contains the PID of the firewall process */

        char *fwblacklist;           /**< Which fw dest. blacklisted IP addr should be registered into */
        char *fwblacklist_sendto;    /**< Which fw dest. blacklisted IP addr should forward their hits to */
        eurephiaVALUES *blacklisted; /**< Contains all IP addresses we have blacklisted */
} eurephiaFWINTF;


/**
 *  Valid firewall modes to be passed over the POSIX MQ queue
 */
typedef enum {fwADD,            /**< Add a new VPN client to the firewall VPN chain */
              fwDELETE,         /**< Delete an existing VPN client from the firewall VPN chain */
              fwBLACKLIST,      /**< Blacklist an IP address */
              fwFLUSH,          /**< Flush the firewall VPN chain */
              fwINITIALISE,     /**< Initialiase the firewall VPN chain */
              fwSHUTDOWN,       /**< Initialiase the firewall VPN chain */
} eFWmode;


/**
 *  Update request for the firewall plug-in
 */
typedef struct {
        eFWmode mode;               /**< Defines what the update will do with the firewall (mandatory) */
        char ipaddress[36];         /**< IP address to include (used by fwADD,fwDELETE and fwBLACKLIST) */
        char macaddress[19];        /**< VPN MAC address (used by fwADD and fwDELETE) */
        char rule_destination[66];  /**< Where this new rule will be inserted (mandatory) */
        char goto_destination[66];  /**< Where matching rules should go next (used by fwBLACKLIST) */
} eFWupdateRequest;

#endif      /* !EUREPHIAFW_STRUCT_H_ */
