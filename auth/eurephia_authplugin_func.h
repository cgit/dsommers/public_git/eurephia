/*  eurephia_authplugin_func.h
 *
 *  Copyright (C) 2013          David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file   eurephia_authplugin_func.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2013-02-15
 *
 * @brief  Authentication plug-in framework for eurephia
 */

#ifndef EUREPHIA_AUTHPLUGIN_FUNC_H_
#define EUREPHIA_AUTHPLUGIN_FUNC_H_

#if !defined(EUREPHIA_AUTHPLUGIN_CONTEXT_H_) && !defined(EUREPHIA_AUTHPLUGIN_DRIVER_H_)
#error eurephia_authplugin_func.h depends on eurephia_authplugin_contxt.h or eurephia_authplugin_driver.h
#endif

#ifdef EUREPHIA_AUTHPLUGIN_CONTEXT_H_
/**
 *  Initialises the eAuthPluginCTX and loads all configured auth plug-ins
 *
 *  @param eurephiaCTX*   Pointer to the global eurephiaCTX.  A database driver
 *                        must already be initialised
 *
 *  @return Returns a pointer to a eAuthPluginCTX on success, otherwise NULL.
 */
eAuthPluginCTX * eAuthPlugin_Init(eurephiaCTX * ctx);

/**
 *  Closes and frees up the memory used by eAuthPluginCTX
 *
 *  @param eAuthPluginCTX*  Pointer to the eAuthPluginCTX to free
 *
 *  @return Returns 1 on success, otherwise 0
 */
void _eAuthPlugin_Close(eurephiaCTX *ctx, eAuthPluginCTX *apctx);
#define eAuthPlugin_Close(c, apc) { _eAuthPlugin_Close(c, apc); apc = NULL; };
#endif /* EUREPHIA_AUTHPLUGIN_CONTEXT_H_ */


#ifdef EUREPHIA_AUTHPLUGIN_DRIVER_H_
/**
 *  Retrieves a plug-in API based on the plug-in name.
 *
 *  @param eAuthPluginCTX*  Pointer to the authentication plugin context
 *  @param char*            Name of the auth-plugin to look up
 *
 *  @return Returns a pointer to the plug-in API on success, otherwise NULL
 */
eAuthPlugin * eAuthPlugin_Get(eAuthPluginCTX *apctx, const int plgid);

ePluginInfoRW * ePluginInspect(eurephiaCTX * ctx, const char * dlfilename);
void ePluginInfoFree(eurephiaCTX *ctx, ePluginInfoRW *plginf);

#endif /* EUREPHIA_AUTHPLUGIN_DRIVER_H_ */

#endif /* EUREPHIA_AUTHPLUGIN_FUNC_H */
