#  cmake rules for eurephia - dummy-auth plug-in
#
#  This module is only for testing purpose.
#  DO NO USE THIS PLUG-IN IN A PRODUCTION ENVIRONMENT
#
#  GPLv2 only - Copyright (C) 2008 - 2012
#               David Sommerseth <dazo@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; version 2
#  of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
PROJECT(socket-auth C)
cmake_minimum_required(VERSION 2.6)

# Compiler settigns
INCLUDE_DIRECTORIES(. ../../common .. ../../database)

ADD_LIBRARY(socket-auth SHARED
         socket-auth.c
)
TARGET_LINK_LIBRARIES(socket-auth common)
SET_TARGET_PROPERTIES(socket-auth PROPERTIES COMPILE_FLAGS -fPIC)
SET_TARGET_PROPERTIES(socket-auth PROPERTIES OUTPUT_NAME socket-auth PREFIX "")

