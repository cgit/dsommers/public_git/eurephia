/*  eurephia_authplugin_driver.h
 *
 *  Copyright (C) 2013          David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file   eurephia_authplugin_driver.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2013-02-15
 *
 * @brief  Authentication plug-in framework for eurephia
 */

#ifndef EUREPHIA_AUTHPLUGIN_DRIVER_H_
#define EUREPHIA_AUTHPLUGIN_DRIVER_H_

/**
 *  Supported plug-in types
 */
typedef enum {
        eptAUTH  /**< Used by authentication plug-ins */
} ePluginType;

/**
 *   Translation table for ePluginType into a readable string
 *   This table must match the ePluginType enumeration.
 */
static const char *___ePluginTypeString[] = {
        "authentication",  /* eptAUTH */
        NULL
};

static const char *___ePluginTypeShortString[] = {
        "auth",  /* eptAUTH */
        NULL
};


/**
 *  Struct providing plug-in information
 */
typedef const struct __ePluginInfo {
        const char *name;             /**< Readable plug-in name */
        const char *version;          /**< Plug-in version string */
        const char *copyright;        /**< Copyright of the plug-in */
        const ePluginType pluginType; /**< Plug-in type */
        const int APIversion;         /**< Plug-in API level support */
} ePluginInfo;


/**
 *  Struct providing plug-in information, a "read-write"
 *  version of ePluginInfo, used by ePluginInspect()
 */
typedef struct __ePluginInfoRW {
        char *name;             /**< Readable plug-in name */
        char *version;          /**< Plug-in version string */
        char *copyright;        /**< Copyright of the plug-in */
        ePluginType pluginType; /**< Plug-in type */
        int APIversion;         /**< Plug-in API level support */
} ePluginInfoRW;


/**
 *  Simple function for getting a readable string of the plug-in type.
 *
 *  @return Returns a char pointer to a static const buffer containing the
 *          plug-in type.
 */
static inline const char * ePluginTypeString(ePluginInfo *plginf)
{
        return ___ePluginTypeString[plginf->pluginType];
}

/**
 *  Simple function for getting a readable short string of the plug-in type.
 *
 *  @return Returns a char pointer to a static const buffer containing the
 *          plug-in type.
 */
static inline const char * ePluginTypeShortString(ePluginInfo *plginf)
{
        return ___ePluginTypeShortString[plginf->pluginType];
}


/**
 *  Authentication result codes
 */
typedef enum { eAUTH_FAILED,     /**< Authentication failed */
               eAUTH_SUCCESS,    /**< Authentication successful */
               eAUTH_PLGERROR    /**< An error occured in the auth plug-in */
} eAuthResStatus;

/**
 *  Result type for authentication plug-ins
 */
typedef struct __eAuthResult {
        eAuthResStatus status;   /**< Overall status of the operation */
        char *msg;               /**< Optinal descriptive information */
} eAuthResult;


/**
 *   Authentication plug-in API
 */
typedef struct __eAuthPlugin {
        /**
         *  Returns information about the authentication plug-in.  All
         *  authentication plug-ins must provide this function.
         *
         *  @return Will always return a pointer to a const struct containing
         *          the plug-in information.
         */
         ePluginInfo * (*PluginInfo)();

        /**
         *  Initialisation function for the plug-in.
         *
         *  @param eurephiaCTX*  Pointer to the current eurephia context
         *  @param args  String containing the initialisation data
         *
         *  @return Returns 1 on success, otherwise 0
         */
        int (*PluginInit)(eurephiaCTX *ctx, const char *args);


        /**
         *  Function for "closing" for the plug-in.
         *
         *  @param eurephiaCTX*  Pointer to the current eurephia context
         *
         */
        void (*PluginClose)(eurephiaCTX *ctx);


        /**
         * Mandatory auth plug-in function.  Authenticates a user account based
         * on the given username and password.
         *
         * @param eurephiaCTX*  Pointer to the current eurephia context
         * @param username*     String pointer to the username for the authentication
         * @param password*     String pointer to the password for the authentication
         *
         * @returns On success it returns a pointer to an eAuthResult struct, which
         *          contains further information on the success of the authentication.
         *          On system failure, NULL is returned.
         */
        eAuthResult * (*AuthenticateUser)(eurephiaCTX *ctx,
                                   const char *username,
                                   const char *password);

        /**
         *  ChangePassword() is only enabled if allowed by config and supported
         *  by (optionally) by the plug-in.  The eurephia context must be in
         *  an admin mode for this to be enabled.
         *
         * @param eurephiaCTX*  Pointer to the current eurephia context
         * @param username*     String pointer to the username account for the password change
         * @param oldpwd*       String pointer to the old password, for authentication
         * @param newpwd*       String pointer to the new password
         *
         * @returns On success it returns a pointer to an eAuthResult struct, which
         *          contains further information on the success of the authentication.
         *          On system failure, NULL is returned.
         */
        eAuthResult * (*ChangePassword)(eurephiaCTX *ctx,
                                 const char *username,
                                 const char *oldpwd,
                                 const char *newpwd);
} eAuthPlugin;

#endif /* EUREPHIA_AUTHPLUGIN_DRIVER_H_ */
