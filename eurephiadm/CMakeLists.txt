#  cmake rules for eurephia - eurephiadm, console admin utility
#
#  GPLv2 only - Copyright (C) 2008 - 2013
#               David Sommerseth <dazo@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; version 2
#  of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
PROJECT(eurephiadm C)
cmake_minimum_required(VERSION 2.6)

# Compiler settings
INCLUDE_DIRECTORIES(../common ../auth ../database)

# eurephiadm parameters
SET(EUREPHIADM_XSLT_PATH "${XSLTROOT}/eurephiadm/" CACHE STRING "Path for eurephiadm XSLT templates")
ADD_DEFINITIONS(-DEUREPHIADM_XSLT_PATH="${EUREPHIADM_XSLT_PATH}")

IF(FIREWALL)
        ADD_DEFINITIONS(-DFIREWALL)
ENDIF(FIREWALL)

ADD_CUSTOM_COMMAND(
        OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/buildinfo.h
        COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/mk_buildinfo.sh
        DEPENDS commands/buildinfo.c
        COMMENT "Creating buildinfo.h"
)
SET_DIRECTORY_PROPERTIES(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES buildinfo.h)

# Build rules for eurephiadm
ADD_EXECUTABLE(eurephiadm
        eurephiadm.c
        argparser.c
        get_console_input.c
        client_config.c
        client_context.c
        client_session.c
        parse_certificate_files.c
        xsltparser.c
        commands/users.c
        commands/lastlog.c
        commands/attempts.c
        commands/blacklist.c
        commands/certificates.c
        commands/usercerts.c
        commands/adminaccess.c
        commands/edit_config.c
        commands/fwprofiles.c
        buildinfo.h
        commands/buildinfo.c
        commands/plugins.c
        ../common/eurephiadb_session_common.c
        ../auth/eurephia_authplugin_driver.c
)

# Man pages to install
SET(MAN_PAGES
  man/eurephiadm.7
  man/eurephiadm-adminaccess.7
  man/eurephiadm-attempts.7
  man/eurephiadm-blacklist.7
  man/eurephiadm-certs.7
  man/eurephiadm-config.7
  man/eurephiadm-fwprofiles.7
  man/eurephiadm-lastlog.7
  man/eurephiadm-usercerts.7
  man/eurephiadm-users.7
)

# Link in the following dynamic libraries with eurephiadm.
# ${EXTRA_LIBS} contains libxml2, libxslt and openssl library names
TARGET_LINK_LIBRARIES(eurephiadm crypto common ${EXTRA_LIBS})

# Install rules for eurephiadm
INSTALL(TARGETS eurephiadm RUNTIME DESTINATION ${BINDIR})
INSTALL(FILES ${MAN_PAGES} DESTINATION ${MANDIR}/man7)

# Also run the rules for the XSLT templates eurephiadm needs
SUBDIRS(../xslt/eurephiadm)
