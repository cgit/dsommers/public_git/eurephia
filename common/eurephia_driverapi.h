/* eurephia_driver.h
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia_driverapi.h
 * @author David Sommerseth <davids@redhat.com>
 * @date   Wed Nov  4 15:23:47 2009
 *
 * @brief  Generic definitions used to provide a driver API
 *
 */

#ifndef _EUREPHIA_DRIVER_H
#define _EUREPHIA_DRIVER_H

#ifndef DRIVER_MODE

/**
 * Function declaration for eurephia drivers.  A better way to export driver functions
 * correctly.
 *
 * @param funcname  Name of the function in the driver
 *
 */
#define EUREPHIA_DRIVERAPI_FUNC(funcname) (*funcname)

#else // ifdef DRIVER_MODE

#define EUREPHIA_DRIVERAPI_FUNC(funcname) funcname

#endif // ifdef DRIVER_MODE
#endif // ifndef _EUREPHIA_DRIVER_H
