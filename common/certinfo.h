/* certinfo.h  --  Structure used when parsing X509 identification string
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   certinfo.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  Functions for parsing X.509 subject information
 *
 */

#ifndef __CERTINFO_H_
#define __CERTINFO_H_

/**
 *  This structure contains certificate information in a structured way.
 */
typedef struct _certinfo {
        char *digest;		/**< Contains the SHA1 fingerprint (digest) of the certificate */
        char *org;		/**< Contains the O (organisation) field from the X.509 certificate */
        char *common_name;	/**< Contains the CN (Common Name) field from the X.509 certificate*/
        char *email;		/**< Contains the emailAddress field from the X.509 certificate */
} certinfo;

certinfo *parse_tlsid(const char *, const char *);
certinfo *parse_x509_cert (X509 *cert);

void free_certinfo(certinfo *);

#endif
