/* eurephiadb_session_common.h  --  Common function for handling sessions
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadb_session_common.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-11-28
 *
 * @brief  Common functions for handling eurephia sessions.
 *
 */

#ifndef   	EUREPHIADB_SESSION_COMMON_H_
#define   	EUREPHIADB_SESSION_COMMON_H_

#include <eurephia_values.h>

int eDBset_session_value(eurephiaCTX *ctx, eurephiaSESSION *session, const char *key, const char *val);

/**
 * Wrapper function for retrieving a value from a session variable
 *
 * @param s eurephiaSESSION pointer
 * @param k key of the value to look for
 *
 * @return Returns a string (char *) containing the value of the key, or NULL on failure.
 */
#define eDBget_session_value(s, k) eGet_value(s->sessvals, k);


/**
 * Front-end function for eDBfree_session_func().  Frees the memory used by an eurephiaSESSION struct
 * and sets the freed pointer to NULL
 *
 * @param c eurephiaCTX
 * @param s eurephiaSESSION pointer which is being freed.
 */
#define eDBfree_session(c, s) { eDBfree_session_func(c, s); s = NULL; }
void eDBfree_session_func(eurephiaCTX *ctx, eurephiaSESSION *sk);

#endif 	    /* !EUREPHIADB_SESSION_COMMON_H_ */
