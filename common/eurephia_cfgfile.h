/* eurephia_cfgfile.h
 *
 *  Simple generic ini-style config file parser
 *
 *  GPLv2 only - Copyright (C) 2011 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia_cfgfile.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2011-07-23
 *
 * @brief A simple, generic ini-style config file parser
 *
 */

#ifndef EUREPHIA_CFGFILE_H_
#define EUREPHIA_CFGFILE_H_

eurephiaVALUES *ecfg_ReadConfig(eurephiaCTX *ctx, const char *cfgname);

#endif /* EUREPHIA_CFGFILE_H_ */
